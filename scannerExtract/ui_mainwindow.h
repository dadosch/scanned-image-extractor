/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionLoad_Directory;
    QAction *action_Set_Target_Dir;
    QAction *action_Quit;
    QAction *action_About;
    QAction *actionAbout_Qt;
    QAction *actionOnline_Help;
    QAction *actionSupport_by_donation;
    QAction *actionCheck_for_new_version;
    QAction *action_Settings;
    QAction *actionAbout_liblbfgs;
    QAction *actionAbout_Open_CV;
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout_3;
    QSplitter *splitter;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QLabel *label_filename;
    QGraphicsView *graphicsView_Source;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_3;
    QToolButton *toolButton_zoomIn;
    QToolButton *toolButton_zoomOut;
    QToolButton *toolButton_zoomOriginal;
    QToolButton *toolButton_zoomFit;
    QSpacerItem *horizontalSpacer;
    QToolButton *toolButton_firstImage;
    QToolButton *toolButton_prevImage;
    QLabel *label_imagePosition;
    QToolButton *toolButton_nextImage;
    QToolButton *toolButton_lastImage;
    QSpacerItem *horizontalSpacer_2;
    QToolButton *toolButton_saveTargtes;
    QSpacerItem *horizontalSpacer_8;
    QToolButton *toolButton_refresh;
    QSpacerItem *horizontalSpacer_10;
    QWidget *verticalLayoutWidget_2;
    QVBoxLayout *verticalLayout_2;
    QGroupBox *groupBox_aspect;
    QGridLayout *gridLayout_2;
    QSpacerItem *horizontalSpacer_9;
    QRadioButton *radioButton_1_1;
    QRadioButton *radioButton_5_4;
    QRadioButton *radioButton_3_2;
    QRadioButton *radioButton_2_1;
    QRadioButton *radioButton_4_1;
    QRadioButton *radioButton_5_3;
    QRadioButton *radioButton_manual;
    QRadioButton *radioButton_4_3;
    QLineEdit *lineEdit_aspectRatio;
    QRadioButton *radioButton_16_9;
    QRadioButton *radioButton_3_1;
    QRadioButton *radioButton_free;
    QCheckBox *checkBox_enforceAspect;
    QGroupBox *groupBox_orientation;
    QGridLayout *gridLayout_3;
    QSpacerItem *horizontalSpacer_5;
    QRadioButton *radioButton_rot0;
    QRadioButton *radioButton_rot180;
    QRadioButton *radioButton_rot90;
    QRadioButton *radioButton_rot270;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout;
    QSpacerItem *horizontalSpacer_6;
    QDoubleSpinBox *doubleSpinBox_crop;
    QDoubleSpinBox *doubleSpinBox_cropInitial;
    QLabel *label_3;
    QLabel *label_4;
    QToolButton *toolButton_Help;
    QFrame *line;
    QLabel *label_2;
    QGraphicsView *graphicsView_Target;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_7;
    QToolButton *toolButton_prevItem;
    QLabel *label_targetPosition;
    QToolButton *toolButton_nextItem;
    QToolButton *toolButton_deleteItem;
    QSpacerItem *horizontalSpacer_4;
    QMenuBar *menubar;
    QMenu *menu_File;
    QMenu *menuHelp;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(1167, 667);
        actionLoad_Directory = new QAction(MainWindow);
        actionLoad_Directory->setObjectName(QString::fromUtf8("actionLoad_Directory"));
        action_Set_Target_Dir = new QAction(MainWindow);
        action_Set_Target_Dir->setObjectName(QString::fromUtf8("action_Set_Target_Dir"));
        action_Quit = new QAction(MainWindow);
        action_Quit->setObjectName(QString::fromUtf8("action_Quit"));
        action_About = new QAction(MainWindow);
        action_About->setObjectName(QString::fromUtf8("action_About"));
        actionAbout_Qt = new QAction(MainWindow);
        actionAbout_Qt->setObjectName(QString::fromUtf8("actionAbout_Qt"));
        actionOnline_Help = new QAction(MainWindow);
        actionOnline_Help->setObjectName(QString::fromUtf8("actionOnline_Help"));
        actionSupport_by_donation = new QAction(MainWindow);
        actionSupport_by_donation->setObjectName(QString::fromUtf8("actionSupport_by_donation"));
        actionCheck_for_new_version = new QAction(MainWindow);
        actionCheck_for_new_version->setObjectName(QString::fromUtf8("actionCheck_for_new_version"));
        action_Settings = new QAction(MainWindow);
        action_Settings->setObjectName(QString::fromUtf8("action_Settings"));
        actionAbout_liblbfgs = new QAction(MainWindow);
        actionAbout_liblbfgs->setObjectName(QString::fromUtf8("actionAbout_liblbfgs"));
        actionAbout_Open_CV = new QAction(MainWindow);
        actionAbout_Open_CV->setObjectName(QString::fromUtf8("actionAbout_Open_CV"));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        verticalLayout_3 = new QVBoxLayout(centralwidget);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(3, 3, 3, 3);
        splitter = new QSplitter(centralwidget);
        splitter->setObjectName(QString::fromUtf8("splitter"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(splitter->sizePolicy().hasHeightForWidth());
        splitter->setSizePolicy(sizePolicy);
        splitter->setOrientation(Qt::Horizontal);
        verticalLayoutWidget = new QWidget(splitter);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(verticalLayoutWidget);
        label->setObjectName(QString::fromUtf8("label"));
        QSizePolicy sizePolicy1(QSizePolicy::Maximum, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy1);

        horizontalLayout->addWidget(label);

        label_filename = new QLabel(verticalLayoutWidget);
        label_filename->setObjectName(QString::fromUtf8("label_filename"));

        horizontalLayout->addWidget(label_filename);


        verticalLayout->addLayout(horizontalLayout);

        graphicsView_Source = new QGraphicsView(verticalLayoutWidget);
        graphicsView_Source->setObjectName(QString::fromUtf8("graphicsView_Source"));

        verticalLayout->addWidget(graphicsView_Source);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_3);

        toolButton_zoomIn = new QToolButton(verticalLayoutWidget);
        toolButton_zoomIn->setObjectName(QString::fromUtf8("toolButton_zoomIn"));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/View-zoom-in.svg"), QSize(), QIcon::Normal, QIcon::Off);
        toolButton_zoomIn->setIcon(icon);
        toolButton_zoomIn->setIconSize(QSize(32, 32));

        horizontalLayout_2->addWidget(toolButton_zoomIn);

        toolButton_zoomOut = new QToolButton(verticalLayoutWidget);
        toolButton_zoomOut->setObjectName(QString::fromUtf8("toolButton_zoomOut"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/View-zoom-out.svg"), QSize(), QIcon::Normal, QIcon::Off);
        toolButton_zoomOut->setIcon(icon1);
        toolButton_zoomOut->setIconSize(QSize(32, 32));

        horizontalLayout_2->addWidget(toolButton_zoomOut);

        toolButton_zoomOriginal = new QToolButton(verticalLayoutWidget);
        toolButton_zoomOriginal->setObjectName(QString::fromUtf8("toolButton_zoomOriginal"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/images/View-zoom-1.svg"), QSize(), QIcon::Normal, QIcon::Off);
        toolButton_zoomOriginal->setIcon(icon2);
        toolButton_zoomOriginal->setIconSize(QSize(32, 32));

        horizontalLayout_2->addWidget(toolButton_zoomOriginal);

        toolButton_zoomFit = new QToolButton(verticalLayoutWidget);
        toolButton_zoomFit->setObjectName(QString::fromUtf8("toolButton_zoomFit"));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/images/View-zoom-fit.svg"), QSize(), QIcon::Normal, QIcon::Off);
        toolButton_zoomFit->setIcon(icon3);
        toolButton_zoomFit->setIconSize(QSize(32, 32));

        horizontalLayout_2->addWidget(toolButton_zoomFit);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        toolButton_firstImage = new QToolButton(verticalLayoutWidget);
        toolButton_firstImage->setObjectName(QString::fromUtf8("toolButton_firstImage"));
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/images/Media-skip-backward.svg"), QSize(), QIcon::Normal, QIcon::Off);
        toolButton_firstImage->setIcon(icon4);
        toolButton_firstImage->setIconSize(QSize(32, 32));

        horizontalLayout_2->addWidget(toolButton_firstImage);

        toolButton_prevImage = new QToolButton(verticalLayoutWidget);
        toolButton_prevImage->setObjectName(QString::fromUtf8("toolButton_prevImage"));
        QIcon icon5;
        icon5.addFile(QString::fromUtf8(":/images/Media-seek-backward.svg"), QSize(), QIcon::Normal, QIcon::Off);
        toolButton_prevImage->setIcon(icon5);
        toolButton_prevImage->setIconSize(QSize(32, 32));

        horizontalLayout_2->addWidget(toolButton_prevImage);

        label_imagePosition = new QLabel(verticalLayoutWidget);
        label_imagePosition->setObjectName(QString::fromUtf8("label_imagePosition"));

        horizontalLayout_2->addWidget(label_imagePosition);

        toolButton_nextImage = new QToolButton(verticalLayoutWidget);
        toolButton_nextImage->setObjectName(QString::fromUtf8("toolButton_nextImage"));
        QIcon icon6;
        icon6.addFile(QString::fromUtf8(":/images/Media-seek-forward.svg"), QSize(), QIcon::Normal, QIcon::Off);
        toolButton_nextImage->setIcon(icon6);
        toolButton_nextImage->setIconSize(QSize(32, 32));

        horizontalLayout_2->addWidget(toolButton_nextImage);

        toolButton_lastImage = new QToolButton(verticalLayoutWidget);
        toolButton_lastImage->setObjectName(QString::fromUtf8("toolButton_lastImage"));
        QIcon icon7;
        icon7.addFile(QString::fromUtf8(":/images/Media-skip-forward.svg"), QSize(), QIcon::Normal, QIcon::Off);
        toolButton_lastImage->setIcon(icon7);
        toolButton_lastImage->setIconSize(QSize(32, 32));

        horizontalLayout_2->addWidget(toolButton_lastImage);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);

        toolButton_saveTargtes = new QToolButton(verticalLayoutWidget);
        toolButton_saveTargtes->setObjectName(QString::fromUtf8("toolButton_saveTargtes"));
        QIcon icon8;
        icon8.addFile(QString::fromUtf8(":/images/Document-save.svg"), QSize(), QIcon::Normal, QIcon::Off);
        toolButton_saveTargtes->setIcon(icon8);
        toolButton_saveTargtes->setIconSize(QSize(32, 32));

        horizontalLayout_2->addWidget(toolButton_saveTargtes);

        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_8);

        toolButton_refresh = new QToolButton(verticalLayoutWidget);
        toolButton_refresh->setObjectName(QString::fromUtf8("toolButton_refresh"));
        QIcon icon9;
        icon9.addFile(QString::fromUtf8(":/images/Refresh_file.svg"), QSize(), QIcon::Normal, QIcon::Off);
        toolButton_refresh->setIcon(icon9);
        toolButton_refresh->setIconSize(QSize(32, 32));

        horizontalLayout_2->addWidget(toolButton_refresh);

        horizontalSpacer_10 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_10);


        verticalLayout->addLayout(horizontalLayout_2);

        splitter->addWidget(verticalLayoutWidget);
        verticalLayoutWidget_2 = new QWidget(splitter);
        verticalLayoutWidget_2->setObjectName(QString::fromUtf8("verticalLayoutWidget_2"));
        verticalLayout_2 = new QVBoxLayout(verticalLayoutWidget_2);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        groupBox_aspect = new QGroupBox(verticalLayoutWidget_2);
        groupBox_aspect->setObjectName(QString::fromUtf8("groupBox_aspect"));
        gridLayout_2 = new QGridLayout(groupBox_aspect);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setVerticalSpacing(1);
        gridLayout_2->setContentsMargins(-1, 3, -1, 0);
        horizontalSpacer_9 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_9, 0, 12, 1, 1);

        radioButton_1_1 = new QRadioButton(groupBox_aspect);
        radioButton_1_1->setObjectName(QString::fromUtf8("radioButton_1_1"));

        gridLayout_2->addWidget(radioButton_1_1, 0, 2, 1, 1);

        radioButton_5_4 = new QRadioButton(groupBox_aspect);
        radioButton_5_4->setObjectName(QString::fromUtf8("radioButton_5_4"));

        gridLayout_2->addWidget(radioButton_5_4, 2, 4, 1, 1);

        radioButton_3_2 = new QRadioButton(groupBox_aspect);
        radioButton_3_2->setObjectName(QString::fromUtf8("radioButton_3_2"));
        radioButton_3_2->setChecked(true);

        gridLayout_2->addWidget(radioButton_3_2, 0, 4, 1, 1);

        radioButton_2_1 = new QRadioButton(groupBox_aspect);
        radioButton_2_1->setObjectName(QString::fromUtf8("radioButton_2_1"));

        gridLayout_2->addWidget(radioButton_2_1, 0, 6, 1, 1);

        radioButton_4_1 = new QRadioButton(groupBox_aspect);
        radioButton_4_1->setObjectName(QString::fromUtf8("radioButton_4_1"));

        gridLayout_2->addWidget(radioButton_4_1, 0, 10, 1, 1);

        radioButton_5_3 = new QRadioButton(groupBox_aspect);
        radioButton_5_3->setObjectName(QString::fromUtf8("radioButton_5_3"));

        gridLayout_2->addWidget(radioButton_5_3, 2, 2, 1, 1);

        radioButton_manual = new QRadioButton(groupBox_aspect);
        radioButton_manual->setObjectName(QString::fromUtf8("radioButton_manual"));

        gridLayout_2->addWidget(radioButton_manual, 2, 8, 1, 1);

        radioButton_4_3 = new QRadioButton(groupBox_aspect);
        radioButton_4_3->setObjectName(QString::fromUtf8("radioButton_4_3"));

        gridLayout_2->addWidget(radioButton_4_3, 0, 5, 1, 1);

        lineEdit_aspectRatio = new QLineEdit(groupBox_aspect);
        lineEdit_aspectRatio->setObjectName(QString::fromUtf8("lineEdit_aspectRatio"));
        QSizePolicy sizePolicy2(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(lineEdit_aspectRatio->sizePolicy().hasHeightForWidth());
        lineEdit_aspectRatio->setSizePolicy(sizePolicy2);

        gridLayout_2->addWidget(lineEdit_aspectRatio, 2, 10, 1, 2);

        radioButton_16_9 = new QRadioButton(groupBox_aspect);
        radioButton_16_9->setObjectName(QString::fromUtf8("radioButton_16_9"));

        gridLayout_2->addWidget(radioButton_16_9, 2, 5, 1, 1);

        radioButton_3_1 = new QRadioButton(groupBox_aspect);
        radioButton_3_1->setObjectName(QString::fromUtf8("radioButton_3_1"));

        gridLayout_2->addWidget(radioButton_3_1, 0, 8, 1, 1);

        radioButton_free = new QRadioButton(groupBox_aspect);
        radioButton_free->setObjectName(QString::fromUtf8("radioButton_free"));
        radioButton_free->setChecked(false);

        gridLayout_2->addWidget(radioButton_free, 2, 6, 1, 1);

        checkBox_enforceAspect = new QCheckBox(groupBox_aspect);
        checkBox_enforceAspect->setObjectName(QString::fromUtf8("checkBox_enforceAspect"));

        gridLayout_2->addWidget(checkBox_enforceAspect, 3, 2, 1, 7);


        verticalLayout_2->addWidget(groupBox_aspect);

        groupBox_orientation = new QGroupBox(verticalLayoutWidget_2);
        groupBox_orientation->setObjectName(QString::fromUtf8("groupBox_orientation"));
        groupBox_orientation->setCheckable(false);
        gridLayout_3 = new QGridLayout(groupBox_orientation);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        gridLayout_3->setVerticalSpacing(0);
        gridLayout_3->setContentsMargins(-1, 0, -1, 0);
        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_3->addItem(horizontalSpacer_5, 1, 0, 1, 1);

        radioButton_rot0 = new QRadioButton(groupBox_orientation);
        radioButton_rot0->setObjectName(QString::fromUtf8("radioButton_rot0"));

        gridLayout_3->addWidget(radioButton_rot0, 1, 1, 1, 1);

        radioButton_rot180 = new QRadioButton(groupBox_orientation);
        radioButton_rot180->setObjectName(QString::fromUtf8("radioButton_rot180"));

        gridLayout_3->addWidget(radioButton_rot180, 1, 3, 1, 1);

        radioButton_rot90 = new QRadioButton(groupBox_orientation);
        radioButton_rot90->setObjectName(QString::fromUtf8("radioButton_rot90"));

        gridLayout_3->addWidget(radioButton_rot90, 1, 2, 1, 1);

        radioButton_rot270 = new QRadioButton(groupBox_orientation);
        radioButton_rot270->setObjectName(QString::fromUtf8("radioButton_rot270"));

        gridLayout_3->addWidget(radioButton_rot270, 1, 4, 1, 1);


        verticalLayout_2->addWidget(groupBox_orientation);

        groupBox_2 = new QGroupBox(verticalLayoutWidget_2);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        gridLayout = new QGridLayout(groupBox_2);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setVerticalSpacing(3);
        gridLayout->setContentsMargins(-1, 0, -1, 0);
        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_6, 0, 1, 1, 1);

        doubleSpinBox_crop = new QDoubleSpinBox(groupBox_2);
        doubleSpinBox_crop->setObjectName(QString::fromUtf8("doubleSpinBox_crop"));
        doubleSpinBox_crop->setDecimals(1);
        doubleSpinBox_crop->setMaximum(99.000000000000000);
        doubleSpinBox_crop->setSingleStep(1.000000000000000);
        doubleSpinBox_crop->setValue(1.000000000000000);

        gridLayout->addWidget(doubleSpinBox_crop, 0, 6, 1, 1);

        doubleSpinBox_cropInitial = new QDoubleSpinBox(groupBox_2);
        doubleSpinBox_cropInitial->setObjectName(QString::fromUtf8("doubleSpinBox_cropInitial"));
        doubleSpinBox_cropInitial->setDecimals(1);
        doubleSpinBox_cropInitial->setMaximum(99.000000000000000);
        doubleSpinBox_cropInitial->setValue(2.000000000000000);

        gridLayout->addWidget(doubleSpinBox_cropInitial, 0, 4, 1, 1);

        label_3 = new QLabel(groupBox_2);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout->addWidget(label_3, 0, 2, 1, 2);

        label_4 = new QLabel(groupBox_2);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout->addWidget(label_4, 0, 5, 1, 1);

        toolButton_Help = new QToolButton(groupBox_2);
        toolButton_Help->setObjectName(QString::fromUtf8("toolButton_Help"));
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        toolButton_Help->setFont(font);
        toolButton_Help->setIconSize(QSize(32, 32));
        toolButton_Help->setCheckable(false);

        gridLayout->addWidget(toolButton_Help, 0, 0, 1, 1);


        verticalLayout_2->addWidget(groupBox_2);

        line = new QFrame(verticalLayoutWidget_2);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        verticalLayout_2->addWidget(line);

        label_2 = new QLabel(verticalLayoutWidget_2);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        verticalLayout_2->addWidget(label_2);

        graphicsView_Target = new QGraphicsView(verticalLayoutWidget_2);
        graphicsView_Target->setObjectName(QString::fromUtf8("graphicsView_Target"));

        verticalLayout_2->addWidget(graphicsView_Target);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_7);

        toolButton_prevItem = new QToolButton(verticalLayoutWidget_2);
        toolButton_prevItem->setObjectName(QString::fromUtf8("toolButton_prevItem"));
        toolButton_prevItem->setIcon(icon5);
        toolButton_prevItem->setIconSize(QSize(32, 32));

        horizontalLayout_3->addWidget(toolButton_prevItem);

        label_targetPosition = new QLabel(verticalLayoutWidget_2);
        label_targetPosition->setObjectName(QString::fromUtf8("label_targetPosition"));

        horizontalLayout_3->addWidget(label_targetPosition);

        toolButton_nextItem = new QToolButton(verticalLayoutWidget_2);
        toolButton_nextItem->setObjectName(QString::fromUtf8("toolButton_nextItem"));
        toolButton_nextItem->setIcon(icon6);
        toolButton_nextItem->setIconSize(QSize(32, 32));

        horizontalLayout_3->addWidget(toolButton_nextItem);

        toolButton_deleteItem = new QToolButton(verticalLayoutWidget_2);
        toolButton_deleteItem->setObjectName(QString::fromUtf8("toolButton_deleteItem"));
        QIcon icon10;
        icon10.addFile(QString::fromUtf8(":/images/Edit-delete.svg"), QSize(), QIcon::Normal, QIcon::Off);
        toolButton_deleteItem->setIcon(icon10);
        toolButton_deleteItem->setIconSize(QSize(32, 32));

        horizontalLayout_3->addWidget(toolButton_deleteItem);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_4);


        verticalLayout_2->addLayout(horizontalLayout_3);

        splitter->addWidget(verticalLayoutWidget_2);

        verticalLayout_3->addWidget(splitter);

        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 1167, 25));
        menu_File = new QMenu(menubar);
        menu_File->setObjectName(QString::fromUtf8("menu_File"));
        menuHelp = new QMenu(menubar);
        menuHelp->setObjectName(QString::fromUtf8("menuHelp"));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        menubar->addAction(menu_File->menuAction());
        menubar->addAction(menuHelp->menuAction());
        menu_File->addAction(actionLoad_Directory);
        menu_File->addSeparator();
        menu_File->addAction(action_Settings);
        menu_File->addSeparator();
        menu_File->addAction(action_Quit);
        menuHelp->addAction(actionOnline_Help);
        menuHelp->addSeparator();
        menuHelp->addAction(action_About);
        menuHelp->addAction(actionAbout_liblbfgs);
        menuHelp->addAction(actionAbout_Qt);
        menuHelp->addAction(actionAbout_Open_CV);
        menuHelp->addSeparator();
        menuHelp->addAction(actionSupport_by_donation);
        menuHelp->addAction(actionCheck_for_new_version);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        actionLoad_Directory->setText(QApplication::translate("MainWindow", "&Load File", nullptr));
#ifndef QT_NO_STATUSTIP
        actionLoad_Directory->setStatusTip(QApplication::translate("MainWindow", "Load an input image, which usually is a scanned image from an album", nullptr));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        actionLoad_Directory->setWhatsThis(QApplication::translate("MainWindow", "Load an input image, which usually is a scanned image from an album", nullptr));
#endif // QT_NO_WHATSTHIS
#ifndef QT_NO_SHORTCUT
        actionLoad_Directory->setShortcut(QApplication::translate("MainWindow", "Ctrl+L", nullptr));
#endif // QT_NO_SHORTCUT
        action_Set_Target_Dir->setText(QApplication::translate("MainWindow", "Set &Target Directory", nullptr));
        action_Set_Target_Dir->setIconText(QApplication::translate("MainWindow", "Set Output Directory", nullptr));
#ifndef QT_NO_TOOLTIP
        action_Set_Target_Dir->setToolTip(QApplication::translate("MainWindow", "Set Output Directory", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        action_Set_Target_Dir->setStatusTip(QApplication::translate("MainWindow", "Set the directory where the extracted files will be written to", nullptr));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        action_Set_Target_Dir->setWhatsThis(QApplication::translate("MainWindow", "Set the directory where the extracted files will be written to", nullptr));
#endif // QT_NO_WHATSTHIS
#ifndef QT_NO_SHORTCUT
        action_Set_Target_Dir->setShortcut(QApplication::translate("MainWindow", "Ctrl+T", nullptr));
#endif // QT_NO_SHORTCUT
        action_Quit->setText(QApplication::translate("MainWindow", "&Exit", nullptr));
#ifndef QT_NO_STATUSTIP
        action_Quit->setStatusTip(QApplication::translate("MainWindow", "Close the program", nullptr));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        action_Quit->setWhatsThis(QApplication::translate("MainWindow", "Close the program", nullptr));
#endif // QT_NO_WHATSTHIS
        action_About->setText(QApplication::translate("MainWindow", "&About", nullptr));
        actionAbout_Qt->setText(QApplication::translate("MainWindow", "About &Qt", nullptr));
        actionOnline_Help->setText(QApplication::translate("MainWindow", "&Help", nullptr));
#ifndef QT_NO_SHORTCUT
        actionOnline_Help->setShortcut(QApplication::translate("MainWindow", "F1", nullptr));
#endif // QT_NO_SHORTCUT
        actionSupport_by_donation->setText(QApplication::translate("MainWindow", "Support by donation", nullptr));
        actionCheck_for_new_version->setText(QApplication::translate("MainWindow", "Check for new version", nullptr));
        action_Settings->setText(QApplication::translate("MainWindow", "&Settings", nullptr));
        actionAbout_liblbfgs->setText(QApplication::translate("MainWindow", "About liblbfgs", nullptr));
        actionAbout_Open_CV->setText(QApplication::translate("MainWindow", "About Open&CV", nullptr));
        label->setText(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">Scanned Image:</span></p></body></html>", nullptr));
        label_filename->setText(QString());
#ifndef QT_NO_TOOLTIP
        graphicsView_Source->setToolTip(QApplication::translate("MainWindow", "The input image view. Zoom in/out (also use mousewheel). Create new output image by start ing to drag. Manipulate output images by dragging on their borders or corners (with SHIFT+drag it is rotating).", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        graphicsView_Source->setStatusTip(QApplication::translate("MainWindow", "The input image view. Zoom in/out (also use mousewheel). Create new output image by start ing to drag. Manipulate output images by dragging on their borders or corners (with SHIFT+drag it is rotating).", nullptr));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        graphicsView_Source->setWhatsThis(QApplication::translate("MainWindow", "The input image view. Zoom in/out (also use mousewheel). Create new output image by start ing to drag. Manipulate output images by dragging on their borders or corners (with SHIFT+drag it is rotating).", nullptr));
#endif // QT_NO_WHATSTHIS
#ifndef QT_NO_TOOLTIP
        toolButton_zoomIn->setToolTip(QApplication::translate("MainWindow", "Zoom in", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        toolButton_zoomIn->setStatusTip(QApplication::translate("MainWindow", "Zoom in", nullptr));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        toolButton_zoomIn->setWhatsThis(QApplication::translate("MainWindow", "Zoom in", nullptr));
#endif // QT_NO_WHATSTHIS
        toolButton_zoomIn->setText(QApplication::translate("MainWindow", "+", nullptr));
#ifndef QT_NO_TOOLTIP
        toolButton_zoomOut->setToolTip(QApplication::translate("MainWindow", "Zoom out", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        toolButton_zoomOut->setStatusTip(QApplication::translate("MainWindow", "Zoom out", nullptr));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        toolButton_zoomOut->setWhatsThis(QApplication::translate("MainWindow", "Zoom out", nullptr));
#endif // QT_NO_WHATSTHIS
        toolButton_zoomOut->setText(QApplication::translate("MainWindow", "-", nullptr));
#ifndef QT_NO_TOOLTIP
        toolButton_zoomOriginal->setToolTip(QApplication::translate("MainWindow", "Zoom to scale 1:1", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        toolButton_zoomOriginal->setStatusTip(QApplication::translate("MainWindow", "Zoom to scale 1:1", nullptr));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        toolButton_zoomOriginal->setWhatsThis(QApplication::translate("MainWindow", "Zoom to scale 1:1", nullptr));
#endif // QT_NO_WHATSTHIS
        toolButton_zoomOriginal->setText(QApplication::translate("MainWindow", "1:1", nullptr));
#ifndef QT_NO_TOOLTIP
        toolButton_zoomFit->setToolTip(QApplication::translate("MainWindow", "Zoom fit", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        toolButton_zoomFit->setStatusTip(QApplication::translate("MainWindow", "Zoom fit", nullptr));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        toolButton_zoomFit->setWhatsThis(QApplication::translate("MainWindow", "Zoom fit", nullptr));
#endif // QT_NO_WHATSTHIS
        toolButton_zoomFit->setText(QApplication::translate("MainWindow", "fit", nullptr));
#ifndef QT_NO_TOOLTIP
        toolButton_firstImage->setToolTip(QApplication::translate("MainWindow", "Go to first file in the current directory", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        toolButton_firstImage->setStatusTip(QApplication::translate("MainWindow", "Go to first file in the current directory", nullptr));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        toolButton_firstImage->setWhatsThis(QApplication::translate("MainWindow", "Go to first file in the current directory", nullptr));
#endif // QT_NO_WHATSTHIS
        toolButton_firstImage->setText(QApplication::translate("MainWindow", "|<", nullptr));
#ifndef QT_NO_TOOLTIP
        toolButton_prevImage->setToolTip(QApplication::translate("MainWindow", "Go to previous file in the current directory", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        toolButton_prevImage->setStatusTip(QApplication::translate("MainWindow", "Go to previous file in the current directory", nullptr));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        toolButton_prevImage->setWhatsThis(QApplication::translate("MainWindow", "Go to previous file in the current directory", nullptr));
#endif // QT_NO_WHATSTHIS
        toolButton_prevImage->setText(QApplication::translate("MainWindow", "<", nullptr));
#ifndef QT_NO_SHORTCUT
        toolButton_prevImage->setShortcut(QApplication::translate("MainWindow", "Ctrl+V", nullptr));
#endif // QT_NO_SHORTCUT
        label_imagePosition->setText(QString());
#ifndef QT_NO_TOOLTIP
        toolButton_nextImage->setToolTip(QApplication::translate("MainWindow", "Go to next file in the current directory (CTRL+B)", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        toolButton_nextImage->setStatusTip(QApplication::translate("MainWindow", "Go to next file in the current directory (CTRL+B)", nullptr));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        toolButton_nextImage->setWhatsThis(QApplication::translate("MainWindow", "Go to next file in the current directory (CTRL+B)", nullptr));
#endif // QT_NO_WHATSTHIS
        toolButton_nextImage->setText(QApplication::translate("MainWindow", ">", nullptr));
#ifndef QT_NO_SHORTCUT
        toolButton_nextImage->setShortcut(QApplication::translate("MainWindow", "Ctrl+B", nullptr));
#endif // QT_NO_SHORTCUT
#ifndef QT_NO_TOOLTIP
        toolButton_lastImage->setToolTip(QApplication::translate("MainWindow", "Go to last file in the current directory", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        toolButton_lastImage->setStatusTip(QApplication::translate("MainWindow", "Go to last file in the current directory", nullptr));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        toolButton_lastImage->setWhatsThis(QApplication::translate("MainWindow", "Go to last file in the current directory", nullptr));
#endif // QT_NO_WHATSTHIS
        toolButton_lastImage->setText(QApplication::translate("MainWindow", ">|", nullptr));
#ifndef QT_NO_TOOLTIP
        toolButton_saveTargtes->setToolTip(QApplication::translate("MainWindow", "Save all output images for current input image (green=has already been saved)", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        toolButton_saveTargtes->setStatusTip(QApplication::translate("MainWindow", "Save all output images for current input image (green=has already been saved)", nullptr));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        toolButton_saveTargtes->setWhatsThis(QApplication::translate("MainWindow", "Save all output images for current input image (green=has already been saved)", nullptr));
#endif // QT_NO_WHATSTHIS
        toolButton_saveTargtes->setText(QApplication::translate("MainWindow", "save", nullptr));
#ifndef QT_NO_SHORTCUT
        toolButton_saveTargtes->setShortcut(QApplication::translate("MainWindow", "Ctrl+S", nullptr));
#endif // QT_NO_SHORTCUT
        toolButton_refresh->setText(QApplication::translate("MainWindow", "...", nullptr));
#ifndef QT_NO_TOOLTIP
        groupBox_aspect->setToolTip(QApplication::translate("MainWindow", "Set aspect options for extracted images", nullptr));
#endif // QT_NO_TOOLTIP
        groupBox_aspect->setTitle(QApplication::translate("MainWindow", "Aspect Ratio", nullptr));
#ifndef QT_NO_TOOLTIP
        radioButton_1_1->setToolTip(QApplication::translate("MainWindow", "Set aspect of output images to 1:1 (1)", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        radioButton_1_1->setStatusTip(QApplication::translate("MainWindow", "Set aspect of output images to 1:1 (1)", nullptr));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        radioButton_1_1->setWhatsThis(QApplication::translate("MainWindow", "Set aspect of output images to 1:1 (1)", nullptr));
#endif // QT_NO_WHATSTHIS
        radioButton_1_1->setText(QApplication::translate("MainWindow", "1:1", nullptr));
#ifndef QT_NO_SHORTCUT
        radioButton_1_1->setShortcut(QApplication::translate("MainWindow", "1", nullptr));
#endif // QT_NO_SHORTCUT
#ifndef QT_NO_TOOLTIP
        radioButton_5_4->setToolTip(QApplication::translate("MainWindow", "Set aspect of output images to 5:4 (8)", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        radioButton_5_4->setStatusTip(QApplication::translate("MainWindow", "Set aspect of output images to 5:4 (8)", nullptr));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        radioButton_5_4->setWhatsThis(QApplication::translate("MainWindow", "Set aspect of output images to 5:4 (8)", nullptr));
#endif // QT_NO_WHATSTHIS
        radioButton_5_4->setText(QApplication::translate("MainWindow", "5:4", nullptr));
#ifndef QT_NO_SHORTCUT
        radioButton_5_4->setShortcut(QApplication::translate("MainWindow", "8", nullptr));
#endif // QT_NO_SHORTCUT
#ifndef QT_NO_TOOLTIP
        radioButton_3_2->setToolTip(QApplication::translate("MainWindow", "Set aspect of output images to 3:2 (2)", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        radioButton_3_2->setStatusTip(QApplication::translate("MainWindow", "Set aspect of output images to 3:2 (2)", nullptr));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        radioButton_3_2->setWhatsThis(QApplication::translate("MainWindow", "Set aspect of output images to 3:2 (2)", nullptr));
#endif // QT_NO_WHATSTHIS
        radioButton_3_2->setText(QApplication::translate("MainWindow", "3:2", nullptr));
#ifndef QT_NO_SHORTCUT
        radioButton_3_2->setShortcut(QApplication::translate("MainWindow", "2", nullptr));
#endif // QT_NO_SHORTCUT
#ifndef QT_NO_TOOLTIP
        radioButton_2_1->setToolTip(QApplication::translate("MainWindow", "Set aspect of output images to 2:1 (4)", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        radioButton_2_1->setStatusTip(QApplication::translate("MainWindow", "Set aspect of output images to 2:1 (4)", nullptr));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        radioButton_2_1->setWhatsThis(QApplication::translate("MainWindow", "Set aspect of output images to 2:1 (4)", nullptr));
#endif // QT_NO_WHATSTHIS
        radioButton_2_1->setText(QApplication::translate("MainWindow", "2:1", nullptr));
#ifndef QT_NO_SHORTCUT
        radioButton_2_1->setShortcut(QApplication::translate("MainWindow", "4", nullptr));
#endif // QT_NO_SHORTCUT
#ifndef QT_NO_TOOLTIP
        radioButton_4_1->setToolTip(QApplication::translate("MainWindow", "Set aspect of output images to 4:1 (6)", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        radioButton_4_1->setStatusTip(QApplication::translate("MainWindow", "Set aspect of output images to 4:1 (6)", nullptr));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        radioButton_4_1->setWhatsThis(QApplication::translate("MainWindow", "Set aspect of output images to 4:1 (6)", nullptr));
#endif // QT_NO_WHATSTHIS
        radioButton_4_1->setText(QApplication::translate("MainWindow", "4:1", nullptr));
#ifndef QT_NO_SHORTCUT
        radioButton_4_1->setShortcut(QApplication::translate("MainWindow", "6", nullptr));
#endif // QT_NO_SHORTCUT
#ifndef QT_NO_TOOLTIP
        radioButton_5_3->setToolTip(QApplication::translate("MainWindow", "Set aspect of output images to 5:3 (7)", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        radioButton_5_3->setStatusTip(QApplication::translate("MainWindow", "Set aspect of output images to 5:3 (7)", nullptr));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        radioButton_5_3->setWhatsThis(QApplication::translate("MainWindow", "Set aspect of output images to 5:3 (7)", nullptr));
#endif // QT_NO_WHATSTHIS
        radioButton_5_3->setText(QApplication::translate("MainWindow", "5:3", nullptr));
#ifndef QT_NO_SHORTCUT
        radioButton_5_3->setShortcut(QApplication::translate("MainWindow", "7", nullptr));
#endif // QT_NO_SHORTCUT
#ifndef QT_NO_TOOLTIP
        radioButton_manual->setToolTip(QApplication::translate("MainWindow", "Set aspect of output images to manual (0)", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        radioButton_manual->setStatusTip(QApplication::translate("MainWindow", "Set aspect of output images to manual (0)", nullptr));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        radioButton_manual->setWhatsThis(QApplication::translate("MainWindow", "Set aspect of output images to manual (0)", nullptr));
#endif // QT_NO_WHATSTHIS
        radioButton_manual->setText(QApplication::translate("MainWindow", "Manual", nullptr));
#ifndef QT_NO_SHORTCUT
        radioButton_manual->setShortcut(QApplication::translate("MainWindow", "0", nullptr));
#endif // QT_NO_SHORTCUT
#ifndef QT_NO_TOOLTIP
        radioButton_4_3->setToolTip(QApplication::translate("MainWindow", "Set aspect of output images to 4:3 (3)", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        radioButton_4_3->setStatusTip(QApplication::translate("MainWindow", "Set aspect of output images to 4:3 (3)", nullptr));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        radioButton_4_3->setWhatsThis(QApplication::translate("MainWindow", "Set aspect of output images to 4:3 (3)", nullptr));
#endif // QT_NO_WHATSTHIS
        radioButton_4_3->setText(QApplication::translate("MainWindow", "4:3", nullptr));
#ifndef QT_NO_SHORTCUT
        radioButton_4_3->setShortcut(QApplication::translate("MainWindow", "3", nullptr));
#endif // QT_NO_SHORTCUT
#ifndef QT_NO_TOOLTIP
        radioButton_16_9->setToolTip(QApplication::translate("MainWindow", "Set aspect of output images to 16:9 (9)", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        radioButton_16_9->setStatusTip(QApplication::translate("MainWindow", "Set aspect of output images to 16:9 (9)", nullptr));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        radioButton_16_9->setWhatsThis(QApplication::translate("MainWindow", "Set aspect of output images to 16:9 (9)", nullptr));
#endif // QT_NO_WHATSTHIS
        radioButton_16_9->setText(QApplication::translate("MainWindow", "16:9", nullptr));
#ifndef QT_NO_SHORTCUT
        radioButton_16_9->setShortcut(QApplication::translate("MainWindow", "9", nullptr));
#endif // QT_NO_SHORTCUT
#ifndef QT_NO_TOOLTIP
        radioButton_3_1->setToolTip(QApplication::translate("MainWindow", "Set aspect of output images to 3:1 (5)", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        radioButton_3_1->setStatusTip(QApplication::translate("MainWindow", "Set aspect of output images to 3:1 (5)", nullptr));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        radioButton_3_1->setWhatsThis(QApplication::translate("MainWindow", "Set aspect of output images to 3:1 (5)", nullptr));
#endif // QT_NO_WHATSTHIS
        radioButton_3_1->setText(QApplication::translate("MainWindow", "3:1", nullptr));
#ifndef QT_NO_SHORTCUT
        radioButton_3_1->setShortcut(QApplication::translate("MainWindow", "5", nullptr));
#endif // QT_NO_SHORTCUT
#ifndef QT_NO_TOOLTIP
        radioButton_free->setToolTip(QApplication::translate("MainWindow", "Set aspect of output images to free or unconstrained (CTRL+-)", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        radioButton_free->setStatusTip(QApplication::translate("MainWindow", "Set aspect of output images to free or unconstrained (CTRL+-)", nullptr));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        radioButton_free->setWhatsThis(QApplication::translate("MainWindow", "Set aspect of output images to free or unconstrained (CTRL+-)", nullptr));
#endif // QT_NO_WHATSTHIS
        radioButton_free->setText(QApplication::translate("MainWindow", "Free", nullptr));
#ifndef QT_NO_SHORTCUT
        radioButton_free->setShortcut(QApplication::translate("MainWindow", "Ctrl+-", nullptr));
#endif // QT_NO_SHORTCUT
        checkBox_enforceAspect->setText(QApplication::translate("MainWindow", "enforce this ratio in next unprocessed image", nullptr));
        groupBox_orientation->setTitle(QApplication::translate("MainWindow", "Orientation of Output", nullptr));
#ifndef QT_NO_TOOLTIP
        radioButton_rot0->setToolTip(QApplication::translate("MainWindow", "the output orientation change is none (A)", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        radioButton_rot0->setStatusTip(QApplication::translate("MainWindow", "the output orientation change is none (A)", nullptr));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        radioButton_rot0->setWhatsThis(QApplication::translate("MainWindow", "the output orientation change is none (A)", nullptr));
#endif // QT_NO_WHATSTHIS
        radioButton_rot0->setText(QApplication::translate("MainWindow", "0\302\260 ", nullptr));
#ifndef QT_NO_SHORTCUT
        radioButton_rot0->setShortcut(QApplication::translate("MainWindow", "A", nullptr));
#endif // QT_NO_SHORTCUT
#ifndef QT_NO_TOOLTIP
        radioButton_rot180->setToolTip(QApplication::translate("MainWindow", "the output orientation change is 180\302\260  (upside down) - (KEY D)", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        radioButton_rot180->setStatusTip(QApplication::translate("MainWindow", "the output orientation change is 180\302\260  (upside down) - (KEY D)", nullptr));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        radioButton_rot180->setWhatsThis(QApplication::translate("MainWindow", "the output orientation change is 180\302\260  (upside down) - (KEY D)", nullptr));
#endif // QT_NO_WHATSTHIS
        radioButton_rot180->setText(QApplication::translate("MainWindow", "180\302\260 ", nullptr));
#ifndef QT_NO_SHORTCUT
        radioButton_rot180->setShortcut(QApplication::translate("MainWindow", "D", nullptr));
#endif // QT_NO_SHORTCUT
#ifndef QT_NO_TOOLTIP
        radioButton_rot90->setToolTip(QApplication::translate("MainWindow", "the output orientation change is 90\302\260  (rotate right) - (KEY S)", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        radioButton_rot90->setStatusTip(QApplication::translate("MainWindow", "the output orientation change is 90\302\260  (rotate right) - (KEY S)", nullptr));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        radioButton_rot90->setWhatsThis(QApplication::translate("MainWindow", "the output orientation change is 90\302\260  (rotate right) - (KEY S)", nullptr));
#endif // QT_NO_WHATSTHIS
        radioButton_rot90->setText(QApplication::translate("MainWindow", "90\302\260 ", nullptr));
#ifndef QT_NO_SHORTCUT
        radioButton_rot90->setShortcut(QApplication::translate("MainWindow", "S", nullptr));
#endif // QT_NO_SHORTCUT
#ifndef QT_NO_TOOLTIP
        radioButton_rot270->setToolTip(QApplication::translate("MainWindow", "the output orientation change is 180\302\260  (rotate left) - (KEY F)", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        radioButton_rot270->setStatusTip(QApplication::translate("MainWindow", "the output orientation change is 180\302\260  (rotate left) - (KEY F)", nullptr));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        radioButton_rot270->setWhatsThis(QApplication::translate("MainWindow", "the output orientation change is 180\302\260  (rotate left) - (KEY F)", nullptr));
#endif // QT_NO_WHATSTHIS
        radioButton_rot270->setText(QApplication::translate("MainWindow", "270\302\260 ", nullptr));
#ifndef QT_NO_SHORTCUT
        radioButton_rot270->setShortcut(QApplication::translate("MainWindow", "F", nullptr));
#endif // QT_NO_SHORTCUT
        groupBox_2->setTitle(QApplication::translate("MainWindow", "Misc", nullptr));
#ifndef QT_NO_TOOLTIP
        doubleSpinBox_crop->setToolTip(QApplication::translate("MainWindow", "detected images will be cropped by this size", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        doubleSpinBox_crop->setStatusTip(QApplication::translate("MainWindow", "detected images will be cropped by this size", nullptr));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        doubleSpinBox_crop->setWhatsThis(QApplication::translate("MainWindow", "detected images will be cropped by this size", nullptr));
#endif // QT_NO_WHATSTHIS
#ifndef QT_NO_TOOLTIP
        doubleSpinBox_cropInitial->setToolTip(QApplication::translate("MainWindow", "This crop is applied to all new targets. Individual target crops can be adapted by selecting it and change \"current\"", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        doubleSpinBox_cropInitial->setStatusTip(QApplication::translate("MainWindow", "This crop is applied to all new targets. Individual target crops can be adapted by selecting it and change \"current\"", nullptr));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        doubleSpinBox_cropInitial->setWhatsThis(QApplication::translate("MainWindow", "This crop is applied to all new targets. Individual target crops can be adapted by selecting it and change \"current\"", nullptr));
#endif // QT_NO_WHATSTHIS
        label_3->setText(QApplication::translate("MainWindow", "Crop border (%), initial:", nullptr));
        label_4->setText(QApplication::translate("MainWindow", "current:", nullptr));
#ifndef QT_NO_TOOLTIP
        toolButton_Help->setToolTip(QApplication::translate("MainWindow", "After clicking on this button, click on any element to get some short help about it", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        toolButton_Help->setStatusTip(QApplication::translate("MainWindow", "After clicking on this button, click on any element to get some short help about it", nullptr));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        toolButton_Help->setWhatsThis(QApplication::translate("MainWindow", "After clicking on this button, click on any element to get some short help about it", nullptr));
#endif // QT_NO_WHATSTHIS
        toolButton_Help->setText(QApplication::translate("MainWindow", "?", nullptr));
        label_2->setText(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">Selected Output Image (Preview):</span></p></body></html>", nullptr));
#ifndef QT_NO_TOOLTIP
        graphicsView_Target->setToolTip(QApplication::translate("MainWindow", "This is the preview area for the currently selected output image", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        graphicsView_Target->setStatusTip(QApplication::translate("MainWindow", "This is the preview area for the currently selected output image", nullptr));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        graphicsView_Target->setWhatsThis(QApplication::translate("MainWindow", "This is the preview area for the currently selected output image", nullptr));
#endif // QT_NO_WHATSTHIS
#ifndef QT_NO_TOOLTIP
        toolButton_prevItem->setToolTip(QApplication::translate("MainWindow", "Go to previous output image within the current input", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        toolButton_prevItem->setStatusTip(QApplication::translate("MainWindow", "Go to previous output image within the current input", nullptr));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        toolButton_prevItem->setWhatsThis(QApplication::translate("MainWindow", "Go to previous output image within the current input", nullptr));
#endif // QT_NO_WHATSTHIS
        toolButton_prevItem->setText(QApplication::translate("MainWindow", "&prev. Item", nullptr));
#ifndef QT_NO_SHORTCUT
        toolButton_prevItem->setShortcut(QApplication::translate("MainWindow", "N", nullptr));
#endif // QT_NO_SHORTCUT
        label_targetPosition->setText(QString());
#ifndef QT_NO_TOOLTIP
        toolButton_nextItem->setToolTip(QApplication::translate("MainWindow", "Go to next output image within the current input (M)", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        toolButton_nextItem->setStatusTip(QApplication::translate("MainWindow", "Go to next output image within the current input (M)", nullptr));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        toolButton_nextItem->setWhatsThis(QApplication::translate("MainWindow", "Go to next output image within the current input (M)", nullptr));
#endif // QT_NO_WHATSTHIS
        toolButton_nextItem->setText(QApplication::translate("MainWindow", "&next Item", nullptr));
#ifndef QT_NO_SHORTCUT
        toolButton_nextItem->setShortcut(QApplication::translate("MainWindow", "M", nullptr));
#endif // QT_NO_SHORTCUT
#ifndef QT_NO_TOOLTIP
        toolButton_deleteItem->setToolTip(QApplication::translate("MainWindow", "remove the current output image (will not be saved to hard drive)", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        toolButton_deleteItem->setStatusTip(QApplication::translate("MainWindow", "remove the current output image (will not be saved to hard drive)", nullptr));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        toolButton_deleteItem->setWhatsThis(QApplication::translate("MainWindow", "remove the current output image (will not be saved to hard drive)", nullptr));
#endif // QT_NO_WHATSTHIS
        toolButton_deleteItem->setText(QApplication::translate("MainWindow", "&delete Item", nullptr));
        menu_File->setTitle(QApplication::translate("MainWindow", "&File", nullptr));
        menuHelp->setTitle(QApplication::translate("MainWindow", "Help", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
