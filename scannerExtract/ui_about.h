/********************************************************************************
** Form generated from reading UI file 'about.ui'
**
** Created by: Qt User Interface Compiler version 5.12.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ABOUT_H
#define UI_ABOUT_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_DialogAbout
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_content;
    QVBoxLayout *verticalLayout_3;
    QLabel *label_image;
    QSpacerItem *verticalSpacer_3;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_pivottitle;
    QLabel *label_version;
    QLabel *label_2;
    QLabel *label_copyright;
    QLabel *label_license;
    QFrame *line;
    QLabel *label_5;
    QFrame *line_2;
    QLabel *label_donation;
    QFrame *line_3;
    QLabel *label_3;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton_done;

    void setupUi(QDialog *DialogAbout)
    {
        if (DialogAbout->objectName().isEmpty())
            DialogAbout->setObjectName(QString::fromUtf8("DialogAbout"));
        DialogAbout->resize(628, 562);
        QSizePolicy sizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(DialogAbout->sizePolicy().hasHeightForWidth());
        DialogAbout->setSizePolicy(sizePolicy);
        verticalLayout = new QVBoxLayout(DialogAbout);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout_content = new QHBoxLayout();
        horizontalLayout_content->setObjectName(QString::fromUtf8("horizontalLayout_content"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(-1, -1, 10, -1);
        label_image = new QLabel(DialogAbout);
        label_image->setObjectName(QString::fromUtf8("label_image"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(label_image->sizePolicy().hasHeightForWidth());
        label_image->setSizePolicy(sizePolicy1);
        label_image->setMinimumSize(QSize(128, 0));
        label_image->setMaximumSize(QSize(128, 16777215));
        label_image->setFrameShape(QFrame::Panel);
        label_image->setFrameShadow(QFrame::Raised);
        label_image->setMargin(5);

        verticalLayout_3->addWidget(label_image);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer_3);


        horizontalLayout_content->addLayout(verticalLayout_3);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        label_pivottitle = new QLabel(DialogAbout);
        label_pivottitle->setObjectName(QString::fromUtf8("label_pivottitle"));
        QFont font;
        font.setPointSize(24);
        font.setBold(true);
        font.setWeight(75);
        label_pivottitle->setFont(font);

        verticalLayout_2->addWidget(label_pivottitle);

        label_version = new QLabel(DialogAbout);
        label_version->setObjectName(QString::fromUtf8("label_version"));
        QFont font1;
        font1.setBold(true);
        font1.setWeight(75);
        label_version->setFont(font1);

        verticalLayout_2->addWidget(label_version);

        label_2 = new QLabel(DialogAbout);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setWordWrap(true);

        verticalLayout_2->addWidget(label_2);

        label_copyright = new QLabel(DialogAbout);
        label_copyright->setObjectName(QString::fromUtf8("label_copyright"));
        label_copyright->setOpenExternalLinks(true);

        verticalLayout_2->addWidget(label_copyright);

        label_license = new QLabel(DialogAbout);
        label_license->setObjectName(QString::fromUtf8("label_license"));

        verticalLayout_2->addWidget(label_license);

        line = new QFrame(DialogAbout);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        verticalLayout_2->addWidget(line);

        label_5 = new QLabel(DialogAbout);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setFrameShape(QFrame::NoFrame);
        label_5->setFrameShadow(QFrame::Raised);
        label_5->setWordWrap(true);
        label_5->setOpenExternalLinks(true);

        verticalLayout_2->addWidget(label_5);

        line_2 = new QFrame(DialogAbout);
        line_2->setObjectName(QString::fromUtf8("line_2"));
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);

        verticalLayout_2->addWidget(line_2);

        label_donation = new QLabel(DialogAbout);
        label_donation->setObjectName(QString::fromUtf8("label_donation"));
        label_donation->setFrameShape(QFrame::NoFrame);
        label_donation->setFrameShadow(QFrame::Raised);
        label_donation->setWordWrap(true);
        label_donation->setOpenExternalLinks(true);

        verticalLayout_2->addWidget(label_donation);

        line_3 = new QFrame(DialogAbout);
        line_3->setObjectName(QString::fromUtf8("line_3"));
        line_3->setFrameShape(QFrame::HLine);
        line_3->setFrameShadow(QFrame::Sunken);

        verticalLayout_2->addWidget(line_3);

        label_3 = new QLabel(DialogAbout);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setWordWrap(true);

        verticalLayout_2->addWidget(label_3);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        pushButton_done = new QPushButton(DialogAbout);
        pushButton_done->setObjectName(QString::fromUtf8("pushButton_done"));

        horizontalLayout->addWidget(pushButton_done);


        verticalLayout_2->addLayout(horizontalLayout);


        horizontalLayout_content->addLayout(verticalLayout_2);


        verticalLayout->addLayout(horizontalLayout_content);


        retranslateUi(DialogAbout);

        QMetaObject::connectSlotsByName(DialogAbout);
    } // setupUi

    void retranslateUi(QDialog *DialogAbout)
    {
        DialogAbout->setWindowTitle(QApplication::translate("DialogAbout", "About Scanned Image Extractor", nullptr));
        label_image->setText(QString());
        label_pivottitle->setText(QApplication::translate("DialogAbout", "Scanned Image Extractor", nullptr));
        label_version->setText(QApplication::translate("DialogAbout", "Version ", nullptr));
        label_2->setText(QApplication::translate("DialogAbout", "<html><head/><body><p align=\"justify\"><span style=\" font-style:italic;\">Scanned Image Extractor</span> is a programm which allows fast and efficient extraction of multiple photographs of scanned albums</p></body></html>", nullptr));
        label_copyright->setText(QApplication::translate("DialogAbout", "<html><head/><body><p>Copyright 2015, Dominik Rue\303\237 <a href=\"mailto:pivot@dominik-ruess.de\"><span style=\" text-decoration: underline; color:#0000ff;\">scanner-extractor@dominik-ruess.de</span></a></p></body></html>", nullptr));
        label_license->setText(QApplication::translate("DialogAbout", "<html><head/><body><p>License: <span style=\" font-style:italic;\">The GNU General Public License (GPL), Version 3</span></p></body></html>", nullptr));
        label_5->setText(QApplication::translate("DialogAbout", "<html><head/><body><p>Bugs, feature requests or spelling errors can be reported to: <a href=\"https://sourceforge.net/p/scannedimageextractor/tickets/\"><span style=\" text-decoration: underline; color:#0000ff;\">sourceforge.net/p/scannedimageextractor/tickets/ </span></a>(or email).<br/>I'm always looking forward to translations into YOUR language.</p></body></html>", nullptr));
        label_donation->setText(QApplication::translate("DialogAbout", "<html><head/><body><p align=\"justify\">If you like <span style=\" font-style:italic;\">Scanned Image Extractor</span> or if you wish to support the author consider donating</span>: </p><ul style=\"margin-top: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; -qt-list-indent: 1;\"><li style=\" margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">via Sourceforge (Donation for user <span style=\" font-style:italic;\">domsen</span>): <a href=\"https://sourceforge.net/p/scannedimageextractor/donate/\"><span style=\" text-decoration: underline; color:#0000ff;\">sourceforge.net/p/scannedimageextractor/donate/</span></a></li><li align=\"justify\" style=\" margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">personally: contact me at <a href=\"mailto:donate@dominik-ruess.de\"><span style=\" text-decoration: underline; color:#0000ff;\">donate@dominik-ruess.de</span></a></li></ul></body></html>", nullptr));
        label_3->setText(QApplication::translate("DialogAbout", "Credits go to the GNOME project (i.e. the gome icon artists) for the application icon (Creative Commons Attribution-Share Alike 3.0 Unported), which consists of Gnome-image-x-generic.svg and Gnome-scanner.svg.", nullptr));
        pushButton_done->setText(QApplication::translate("DialogAbout", "&Ok", nullptr));
    } // retranslateUi

};

namespace Ui {
    class DialogAbout: public Ui_DialogAbout {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ABOUT_H
