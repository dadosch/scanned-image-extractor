/********************************************************************************
** Form generated from reading UI file 'settingsdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.12.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SETTINGSDIALOG_H
#define UI_SETTINGSDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SettingsDialog
{
public:
    QVBoxLayout *verticalLayout;
    QTabWidget *tabWidget;
    QWidget *tab;
    QVBoxLayout *verticalLayout_2;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_2;
    QLineEdit *lineEdit_prefix;
    QLabel *label_18;
    QLabel *label_15;
    QLineEdit *lineEdit_location;
    QToolButton *toolButton_folder;
    QLabel *label_14;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer_2;
    QSpinBox *spinBox_preload;
    QGroupBox *groupBox_3;
    QGridLayout *gridLayout_3;
    QLabel *label_2;
    QLabel *label_3;
    QSpinBox *spinBox_minArea;
    QLabel *label_4;
    QSpinBox *spinBox_minAreaWithinImage;
    QLabel *label_5;
    QDoubleSpinBox *doubleSpinBox_maxAspect;
    QLabel *label_10;
    QSpinBox *spinBox_maxOverlap;
    QSpacerItem *verticalSpacer_2;
    QWidget *tab_2;
    QVBoxLayout *verticalLayout_3;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QSpinBox *spinBox_splitMinLengthFrac;
    QSpinBox *spinBox_levels;
    QDoubleSpinBox *doubleSpinBox_thres;
    QLabel *label_8;
    QLabel *label_12;
    QLabel *label_6;
    QSpinBox *spinBox_splitMaxOffsetFrac;
    QLabel *label_9;
    QSpinBox *spinBox_splitMinCornerDist;
    QLabel *label_7;
    QLabel *label_11;
    QLabel *label_13;
    QSpacerItem *horizontalSpacer;
    QLabel *label_16;
    QLabel *label_17;
    QSpinBox *spinBox_maxHierarchyLevel;
    QSpacerItem *verticalSpacer;
    QLabel *label;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *SettingsDialog)
    {
        if (SettingsDialog->objectName().isEmpty())
            SettingsDialog->setObjectName(QString::fromUtf8("SettingsDialog"));
        SettingsDialog->resize(565, 583);
        verticalLayout = new QVBoxLayout(SettingsDialog);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        tabWidget = new QTabWidget(SettingsDialog);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabWidget->setEnabled(true);
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        verticalLayout_2 = new QVBoxLayout(tab);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        groupBox_2 = new QGroupBox(tab);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        gridLayout_2 = new QGridLayout(groupBox_2);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        lineEdit_prefix = new QLineEdit(groupBox_2);
        lineEdit_prefix->setObjectName(QString::fromUtf8("lineEdit_prefix"));

        gridLayout_2->addWidget(lineEdit_prefix, 1, 2, 1, 2);

        label_18 = new QLabel(groupBox_2);
        label_18->setObjectName(QString::fromUtf8("label_18"));

        gridLayout_2->addWidget(label_18, 2, 0, 1, 1);

        label_15 = new QLabel(groupBox_2);
        label_15->setObjectName(QString::fromUtf8("label_15"));

        gridLayout_2->addWidget(label_15, 1, 0, 1, 1);

        lineEdit_location = new QLineEdit(groupBox_2);
        lineEdit_location->setObjectName(QString::fromUtf8("lineEdit_location"));

        gridLayout_2->addWidget(lineEdit_location, 0, 2, 1, 1);

        toolButton_folder = new QToolButton(groupBox_2);
        toolButton_folder->setObjectName(QString::fromUtf8("toolButton_folder"));

        gridLayout_2->addWidget(toolButton_folder, 0, 3, 1, 1);

        label_14 = new QLabel(groupBox_2);
        label_14->setObjectName(QString::fromUtf8("label_14"));

        gridLayout_2->addWidget(label_14, 0, 0, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);

        spinBox_preload = new QSpinBox(groupBox_2);
        spinBox_preload->setObjectName(QString::fromUtf8("spinBox_preload"));

        horizontalLayout->addWidget(spinBox_preload);


        gridLayout_2->addLayout(horizontalLayout, 2, 2, 1, 2);


        verticalLayout_2->addWidget(groupBox_2);

        groupBox_3 = new QGroupBox(tab);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        gridLayout_3 = new QGridLayout(groupBox_3);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        label_2 = new QLabel(groupBox_3);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        QFont font;
        font.setBold(false);
        font.setItalic(true);
        font.setWeight(50);
        label_2->setFont(font);
        label_2->setScaledContents(false);

        gridLayout_3->addWidget(label_2, 0, 0, 1, 1);

        label_3 = new QLabel(groupBox_3);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setStyleSheet(QString::fromUtf8("margin-left:10px;"));

        gridLayout_3->addWidget(label_3, 1, 0, 1, 1);

        spinBox_minArea = new QSpinBox(groupBox_3);
        spinBox_minArea->setObjectName(QString::fromUtf8("spinBox_minArea"));

        gridLayout_3->addWidget(spinBox_minArea, 1, 1, 1, 1);

        label_4 = new QLabel(groupBox_3);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setStyleSheet(QString::fromUtf8("margin-left:10px"));
        label_4->setWordWrap(false);

        gridLayout_3->addWidget(label_4, 2, 0, 1, 1);

        spinBox_minAreaWithinImage = new QSpinBox(groupBox_3);
        spinBox_minAreaWithinImage->setObjectName(QString::fromUtf8("spinBox_minAreaWithinImage"));

        gridLayout_3->addWidget(spinBox_minAreaWithinImage, 2, 1, 1, 1);

        label_5 = new QLabel(groupBox_3);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setStyleSheet(QString::fromUtf8("margin-left:10px"));

        gridLayout_3->addWidget(label_5, 3, 0, 1, 1);

        doubleSpinBox_maxAspect = new QDoubleSpinBox(groupBox_3);
        doubleSpinBox_maxAspect->setObjectName(QString::fromUtf8("doubleSpinBox_maxAspect"));
        doubleSpinBox_maxAspect->setDecimals(1);
        doubleSpinBox_maxAspect->setMinimum(1.000000000000000);

        gridLayout_3->addWidget(doubleSpinBox_maxAspect, 3, 1, 1, 1);

        label_10 = new QLabel(groupBox_3);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setStyleSheet(QString::fromUtf8("margin-left:10px"));

        gridLayout_3->addWidget(label_10, 4, 0, 1, 1);

        spinBox_maxOverlap = new QSpinBox(groupBox_3);
        spinBox_maxOverlap->setObjectName(QString::fromUtf8("spinBox_maxOverlap"));

        gridLayout_3->addWidget(spinBox_maxOverlap, 4, 1, 1, 1);


        verticalLayout_2->addWidget(groupBox_3);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_2);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        verticalLayout_3 = new QVBoxLayout(tab_2);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        groupBox = new QGroupBox(tab_2);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        spinBox_splitMinLengthFrac = new QSpinBox(groupBox);
        spinBox_splitMinLengthFrac->setObjectName(QString::fromUtf8("spinBox_splitMinLengthFrac"));

        gridLayout->addWidget(spinBox_splitMinLengthFrac, 6, 2, 1, 1);

        spinBox_levels = new QSpinBox(groupBox);
        spinBox_levels->setObjectName(QString::fromUtf8("spinBox_levels"));
        spinBox_levels->setMinimum(1);

        gridLayout->addWidget(spinBox_levels, 1, 2, 1, 1);

        doubleSpinBox_thres = new QDoubleSpinBox(groupBox);
        doubleSpinBox_thres->setObjectName(QString::fromUtf8("doubleSpinBox_thres"));
        doubleSpinBox_thres->setDecimals(1);
        doubleSpinBox_thres->setMinimum(0.100000000000000);

        gridLayout->addWidget(doubleSpinBox_thres, 2, 2, 1, 1);

        label_8 = new QLabel(groupBox);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        gridLayout->addWidget(label_8, 7, 0, 1, 1);

        label_12 = new QLabel(groupBox);
        label_12->setObjectName(QString::fromUtf8("label_12"));
        label_12->setStyleSheet(QString::fromUtf8("margin-left:10px"));

        gridLayout->addWidget(label_12, 1, 0, 1, 1);

        label_6 = new QLabel(groupBox);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        QFont font1;
        font1.setItalic(true);
        label_6->setFont(font1);

        gridLayout->addWidget(label_6, 5, 0, 1, 1);

        spinBox_splitMaxOffsetFrac = new QSpinBox(groupBox);
        spinBox_splitMaxOffsetFrac->setObjectName(QString::fromUtf8("spinBox_splitMaxOffsetFrac"));

        gridLayout->addWidget(spinBox_splitMaxOffsetFrac, 7, 2, 1, 1);

        label_9 = new QLabel(groupBox);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        gridLayout->addWidget(label_9, 8, 0, 1, 1);

        spinBox_splitMinCornerDist = new QSpinBox(groupBox);
        spinBox_splitMinCornerDist->setObjectName(QString::fromUtf8("spinBox_splitMinCornerDist"));

        gridLayout->addWidget(spinBox_splitMinCornerDist, 8, 2, 1, 1);

        label_7 = new QLabel(groupBox);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        gridLayout->addWidget(label_7, 6, 0, 1, 1);

        label_11 = new QLabel(groupBox);
        label_11->setObjectName(QString::fromUtf8("label_11"));
        label_11->setFont(font1);

        gridLayout->addWidget(label_11, 0, 0, 1, 1);

        label_13 = new QLabel(groupBox);
        label_13->setObjectName(QString::fromUtf8("label_13"));
        label_13->setStyleSheet(QString::fromUtf8("margin-left:10px"));

        gridLayout->addWidget(label_13, 2, 0, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 2, 1, 1, 1);

        label_16 = new QLabel(groupBox);
        label_16->setObjectName(QString::fromUtf8("label_16"));
        label_16->setFont(font1);

        gridLayout->addWidget(label_16, 3, 0, 1, 1);

        label_17 = new QLabel(groupBox);
        label_17->setObjectName(QString::fromUtf8("label_17"));
        label_17->setStyleSheet(QString::fromUtf8("margin-left:10px"));

        gridLayout->addWidget(label_17, 4, 0, 1, 1);

        spinBox_maxHierarchyLevel = new QSpinBox(groupBox);
        spinBox_maxHierarchyLevel->setObjectName(QString::fromUtf8("spinBox_maxHierarchyLevel"));

        gridLayout->addWidget(spinBox_maxHierarchyLevel, 4, 2, 1, 1);


        verticalLayout_3->addWidget(groupBox);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer);

        tabWidget->addTab(tab_2, QString());

        verticalLayout->addWidget(tabWidget);

        label = new QLabel(SettingsDialog);
        label->setObjectName(QString::fromUtf8("label"));
        label->setFont(font1);
        label->setStyleSheet(QString::fromUtf8("margin-right:10px"));
        label->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        verticalLayout->addWidget(label);

        buttonBox = new QDialogButtonBox(SettingsDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        verticalLayout->addWidget(buttonBox);


        retranslateUi(SettingsDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), SettingsDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), SettingsDialog, SLOT(reject()));

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(SettingsDialog);
    } // setupUi

    void retranslateUi(QDialog *SettingsDialog)
    {
        SettingsDialog->setWindowTitle(QApplication::translate("SettingsDialog", "Scanner Extract Settings", nullptr));
        groupBox_2->setTitle(QApplication::translate("SettingsDialog", "Generic Settings", nullptr));
        label_18->setText(QApplication::translate("SettingsDialog", "pre-load so many images:", nullptr));
        label_15->setText(QApplication::translate("SettingsDialog", "image write prefix:", nullptr));
#ifndef QT_NO_TOOLTIP
        toolButton_folder->setToolTip(QApplication::translate("SettingsDialog", "select folder", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        toolButton_folder->setStatusTip(QApplication::translate("SettingsDialog", "select folder", nullptr));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        toolButton_folder->setWhatsThis(QApplication::translate("SettingsDialog", "select folder", nullptr));
#endif // QT_NO_WHATSTHIS
        toolButton_folder->setText(QApplication::translate("SettingsDialog", "...", nullptr));
        label_14->setText(QApplication::translate("SettingsDialog", "write to folder:", nullptr));
        groupBox_3->setTitle(QApplication::translate("SettingsDialog", "Image Extraction Settings (simple)", nullptr));
        label_2->setText(QApplication::translate("SettingsDialog", "Subframe requirements", nullptr));
        label_3->setText(QApplication::translate("SettingsDialog", "minimum subframe area x%, of original image area:", nullptr));
        label_4->setText(QApplication::translate("SettingsDialog", "<html><head/><body><p>minimum subframe area x%, which needs to be within<br/>original image:</p></body></html>", nullptr));
        label_5->setText(QApplication::translate("SettingsDialog", "maximum aspect ratio:", nullptr));
        label_10->setText(QApplication::translate("SettingsDialog", "<html><head/><body><p>if x% of the subframe is contained in another, <br/>choose the larger (area wise):</p></body></html>", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("SettingsDialog", "Generic Settings", nullptr));
        groupBox->setTitle(QApplication::translate("SettingsDialog", "Automated Image Extraction settings (ADVANCED)", nullptr));
        label_8->setStyleSheet(QApplication::translate("SettingsDialog", "margin-left:10px;", nullptr));
        label_8->setText(QApplication::translate("SettingsDialog", "<html><head/><body><p>Maximum displacement from diagonal of the <br/>convexity defect candidates (% of diagonal):</p></body></html>", nullptr));
        label_12->setText(QApplication::translate("SettingsDialog", "<html><head/><body><p>number of levels (the more the stable but also the less <br/>close images may be placed to each other):</p></body></html>", nullptr));
        label_6->setText(QApplication::translate("SettingsDialog", "Rectangle splitting (for diagonally overlapping subframes)", nullptr));
        label_9->setStyleSheet(QApplication::translate("SettingsDialog", "margin-left:10px;", nullptr));
        label_9->setText(QApplication::translate("SettingsDialog", "<html><head/><body><p>overlapping minimum distance from enclosing <br/>rectangle corner (% of diagonal):</p></body></html>", nullptr));
        label_7->setStyleSheet(QApplication::translate("SettingsDialog", "margin-left:10px;", nullptr));
        label_7->setText(QApplication::translate("SettingsDialog", "<html><head/><body><p>sub contour of needs to be longer than x% of the <br/>diagonal length of the enclosing rectangle:</p></body></html>", nullptr));
        label_11->setText(QApplication::translate("SettingsDialog", "Pre-Processing", nullptr));
        label_13->setText(QApplication::translate("SettingsDialog", "threshold for edge image (x times the number of levels):", nullptr));
        label_16->setText(QApplication::translate("SettingsDialog", "Contours", nullptr));
        label_17->setText(QApplication::translate("SettingsDialog", "Maximum contour hierarchy level", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("SettingsDialog", "Advanced", nullptr));
        label->setText(QApplication::translate("SettingsDialog", "Note: Any changes will only apply to unvisited images or if reloaded", nullptr));
    } // retranslateUi

};

namespace Ui {
    class SettingsDialog: public Ui_SettingsDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SETTINGSDIALOG_H
