/********************************************************************************
** Form generated from reading UI file 'helpdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.12.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_HELPDIALOG_H
#define UI_HELPDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_HelpDialog
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *label_2;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QVBoxLayout *verticalLayout_2;
    QLabel *label;
    QLabel *label_3;
    QLabel *label_overview;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label_6;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton;
    QSpacerItem *horizontalSpacer_2;

    void setupUi(QDialog *HelpDialog)
    {
        if (HelpDialog->objectName().isEmpty())
            HelpDialog->setObjectName(QString::fromUtf8("HelpDialog"));
        HelpDialog->resize(750, 650);
        verticalLayout = new QVBoxLayout(HelpDialog);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label_2 = new QLabel(HelpDialog);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        QFont font;
        font.setPointSize(18);
        font.setBold(true);
        font.setWeight(75);
        label_2->setFont(font);

        verticalLayout->addWidget(label_2);

        scrollArea = new QScrollArea(HelpDialog);
        scrollArea->setObjectName(QString::fromUtf8("scrollArea"));
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QString::fromUtf8("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 730, 561));
        verticalLayout_2 = new QVBoxLayout(scrollAreaWidgetContents);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        label = new QLabel(scrollAreaWidgetContents);
        label->setObjectName(QString::fromUtf8("label"));
        label->setOpenExternalLinks(true);

        verticalLayout_2->addWidget(label);

        label_3 = new QLabel(scrollAreaWidgetContents);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setWordWrap(true);

        verticalLayout_2->addWidget(label_3);

        label_overview = new QLabel(scrollAreaWidgetContents);
        label_overview->setObjectName(QString::fromUtf8("label_overview"));
        label_overview->setStyleSheet(QString::fromUtf8(""));
        label_overview->setFrameShape(QFrame::Panel);
        label_overview->setFrameShadow(QFrame::Raised);
        label_overview->setAlignment(Qt::AlignCenter);
        label_overview->setMargin(25);

        verticalLayout_2->addWidget(label_overview);

        label_4 = new QLabel(scrollAreaWidgetContents);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setWordWrap(true);

        verticalLayout_2->addWidget(label_4);

        label_5 = new QLabel(scrollAreaWidgetContents);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setWordWrap(true);

        verticalLayout_2->addWidget(label_5);

        label_6 = new QLabel(scrollAreaWidgetContents);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        verticalLayout_2->addWidget(label_6);

        scrollArea->setWidget(scrollAreaWidgetContents);

        verticalLayout->addWidget(scrollArea);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        pushButton = new QPushButton(HelpDialog);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        horizontalLayout->addWidget(pushButton);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        verticalLayout->addLayout(horizontalLayout);


        retranslateUi(HelpDialog);
        QObject::connect(pushButton, SIGNAL(clicked()), HelpDialog, SLOT(close()));

        QMetaObject::connectSlotsByName(HelpDialog);
    } // setupUi

    void retranslateUi(QDialog *HelpDialog)
    {
        HelpDialog->setWindowTitle(QApplication::translate("HelpDialog", "Help", nullptr));
        label_2->setText(QApplication::translate("HelpDialog", "Scanned Image Extractor Help", nullptr));
        label->setText(QApplication::translate("HelpDialog", "Find the more detailed online help at <a href=\"http://dominik-ruess.de/scannerExtract\">dominik-ruess.de/scannerExtract</a>", nullptr));
        label_3->setText(QApplication::translate("HelpDialog", "This is a short introduction on how to use <i>Scanned Image Extractor</i>. Scroll down to see the complete help text. <br>\n"
"Now, here is an example of how the user interface of the program may look like:", nullptr));
        label_overview->setText(QApplication::translate("HelpDialog", "image", nullptr));
        label_4->setText(QApplication::translate("HelpDialog", "First of all, you load a scanner image. It will appear in the area marked with (1). The program will suggest some photographs. These are marked with a rectangle. Once you select such a rectangle, its preview will apear in the area (2). <br>The properties of these rectangles can be changed in area (3). aspect ratio changes are located in (4) and the orientation of every rectangle/photograph can be changed in (5).", nullptr));
        label_5->setText(QApplication::translate("HelpDialog", "<b>Photograph/Rectangle Handling:</b>\n"
"<br>You can modify the shape of the output photographs/rectangles:<ul>\n"
"<li>drag corner or edge of rectangles for size changes</li>\n"
"              <li>press CTRL for symmetric change</li>\n"
"              <li>keep SHIFT pressed before dragging corner, this rotates the rectangle</li>\n"
"              <li>add new rectangle: deselect all (click somewhere empty). Click on a photograph corner, keep mouse clicked and drag line to a second corner. Then move/resize the new rectangle and click to release.</li>\n"
"              </ul>\n"
"If you process to the next scanned image, the previous photographs will be extracted <i>automatically</i>.", nullptr));
        label_6->setText(QApplication::translate("HelpDialog", "<b>Keyboard shortcuts:</b><table>\n"
"              <tr><td>Keys 0-9</td><td> select aspect ratios</td></tr>\n"
"              <tr><td>Keys 'a', 's', 'd' and 'f'</td><td> change orientation of current target</td></tr>\n"
"              <tr><td style=\"padding-right:20px;\">Keys CTRL+V and CTRL+B</td><td> navigate to prev. and next input image</td></tr>\n"
"              <tr><td>Keys N, M and delete</td><td> navigate prev. and next target or delete target</td></tr></table>", nullptr));
        pushButton->setText(QApplication::translate("HelpDialog", "&OK", nullptr));
    } // retranslateUi

};

namespace Ui {
    class HelpDialog: public Ui_HelpDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_HELPDIALOG_H
